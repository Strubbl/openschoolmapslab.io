ifdef::hoeflichkeitsform[]
Noch Fragen? Wenden Sie sich an die OpenStreetMap-Community (https://osm.ch/hilfe.html)!
endif::[]

ifndef::hoeflichkeitsform[]
Noch Fragen? Wende dich an die OpenStreetMap-Community (https://osm.ch/hilfe.html)!
endif::[]
